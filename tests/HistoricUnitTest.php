<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\HistoricQuestion;
use App\Entity\Question;

class HistoricUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $historicQuestion = new HistoricQuestion();
        $question = new Question;

        $historicQuestion->setTitle('title')
                 ->setStatus('draft')
                 ->setQuestion($question);
        
        $this->assertTrue($historicQuestion->getTitle() === 'title');
        $this->assertTrue($historicQuestion->getStatus() === 'draft');
        $this->assertTrue($historicQuestion->getQuestion() === $question);

    }

    public function testIsFalse(): void
    {
        $historicQuestion = new HistoricQuestion();
        $question = new Question;

        $historicQuestion->setTitle('title')
                 ->setStatus('draft')
                 ->setQuestion($question);
        
        $this->assertFalse($historicQuestion->getTitle() === 'false');
        $this->assertFalse($historicQuestion->getStatus() === 'pubished');
        $this->assertFalse($historicQuestion->getQuestion() === 'false');
    }

    public function testIsEmpty(): void
    {
        $historicQuestion = new HistoricQuestion();
        
        $this->assertEmpty($historicQuestion->getTitle());
        $this->assertEmpty($historicQuestion->getStatus());
        $this->assertEmpty($historicQuestion->getQuestion());
    }
}
