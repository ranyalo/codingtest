<?php

namespace App\Tests;

use App\Entity\Question;
use PHPUnit\Framework\TestCase;

class QuestionUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $question = new Question();

        $question->setTitle('title')
                 ->setPromoted(true)
                 ->setStatus('draft');
        
        $this->assertTrue($question->getTitle() === 'title');
        $this->assertTrue($question->isPromoted() === true);
        $this->assertTrue($question->getStatus() === 'draft');
    }

    public function testIsFalse(): void
    {
        $question = new Question();

        $question->setTitle('title')
                 ->setPromoted(true)
                 ->setStatus('draft');
        
        $this->assertFalse($question->getTitle() === 'title1');
        $this->assertFalse($question->isPromoted() === false);
        $this->assertFalse($question->getStatus() === 'published');
    }

    public function testIsEmpty(): void
    {
        $question = new Question();
        
        $this->assertEmpty($question->getTitle());
        $this->assertEmpty($question->isPromoted());
        $this->assertEmpty($question->getStatus());
    }

}
