<?php
namespace App\DoctrineType;
use App\Channel;

class ChannelType extends AbstractEnumType
{
    public const NAME = 'channel';

    public function getName(): string // the name of the type.
    {
        return self::NAME;
    }

    public static function getEnumsClass(): string // the enums class to convert
    {
        return Channel::class;
    }
}