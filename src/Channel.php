<?php

namespace App;

enum Channel: string {
    case Faq =  'faq';
    case Bot = 'bot';
}