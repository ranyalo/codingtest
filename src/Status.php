<?php

namespace App;

enum Status: string {
    case Draft =  'draft';
    case Published = 'published';
}